// data.js
// example js  file which will split lorem ipsum to an array and print it
//  some lines have trailing   whitespaces in them
const printText = (inputText) => {  
    for (const key of inputText) {    
        console.log(key); // this is a comment
    } 
} 
 
// The line above has an empty spacebar in it so its not empty
const randomText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc non vehicula lorem, non cursus justo. Sed suscipit dui libero, sed mollis tortor lacinia non. Ut tincidunt libero ipsum, accumsan rutrum nisi convallis sed. Maecenas lacinia cursus rutrum. Vivamus sodales risus erat, quis ultricies ipsum maximus sit amet. Cras mauris nibh, malesuada in tellus vel, dignissim feugiat dui. Duis leo enim, tincidunt sit amet pretium ut, mollis ut diam. Aenean ac maximus arcu. Quisque ut lectus sed lectus efficitur bibendum sit amet quis elit. Fusce quis erat sed nunc vehicula porta quis dictum enim. Integer pellentesque, ligula non tincidunt suscipit, erat nisi iaculis ex, quis suscipit magna ex in erat. In laoreet mauris vel vehicula tristique."
/*  text
    comment
    more text
*/ // another comment
printText(randomText.split(" "))
