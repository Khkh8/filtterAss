# FilterAssignment
### Henri Vatto

I created a javascript file data.js which I run through the filters using the command:
```powershell
cat .\data.js | .\teht7.exe > result.js
```
I tested the js program with command:
```powershell
node result.js
``` 
These tests were ran on Windows 10.

## Step 9:
 - The programs 1 and 5 dont work.
   - Program 1 only prints out the calculations of the words, characters etc.
   - Program 5 doesn't work since the spaces are removed. Not sure if thats what i had to do, but thats what i understood from the assignment.
 - Any string that has the comment marks inside of it will have them removed.

#### Screenshot of the step 10 a)
![asd](./image1.png)
#### Screenshot of the step 10 b)
![asd](./image2.png)
#### Screenshot of the step 10 c)
![asd](./image3.png)
#### Screenshot of the compression percentages
![asd](./image4.png)
#### Screenshot of the compression graphs
![asd](./image5.png)
# Source codes:
Step1:
```c++
#include <iostream>
#include <string>
#include <regex>
int main() {
 std::regex const expressionWord("[^\\s]+");
 std::regex const expressionChar(".");
 std::regex const expressionWhitespace("[\\s]");
 int words = 0;
 int characters = 0;
 int lines = 0;
 int whitespaces = 0;
 for (std::string line; std::getline(std::cin, line);) {
 std::ptrdiff_t const match_count(std::distance(
 std::sregex_iterator(line.begin(), line.end(), expressionWord),
 std::sregex_iterator()));
 words += match_count;
 std::ptrdiff_t const match_count2(std::distance(
 std::sregex_iterator(line.begin(), line.end(), expressionChar),
 std::sregex_iterator()));
 characters += match_count2;
 std::ptrdiff_t const match_count3(std::distance(
 std::sregex_iterator(line.begin(), line.end(), expressionWhitespace),
 std::sregex_iterator()));
 whitespaces += match_count3;
 lines++;
 }
 std::cout << "Number of characters: " << characters << std::endl;
 std::cout << "Number of whitespaces: " << whitespaces << std::endl;
 std::cout << "Number of words: " << words << std::endl;
 std::cout << "Number of lines: " << lines << std::endl;
 return 0;
}
```
Step2:
```c++
#include <iostream>
#include <string>
#include <regex>
int main() {
 for (std::string line; std::getline(std::cin, line);) {
 if (!std::regex_match(line, std::regex("^$")))
 std::cout << line << std::endl;
 }
 return 0;
}
```
Step3:
```c++
#include <iostream>
#include <string>
#include <regex>
int main() {
 std::regex regexp("[ \t]+$");
 for (std::string line; std::getline(std::cin, line);) {
 std::string result;
 std::regex_replace(back_inserter(result), line.begin(), line.end(), regexp, "");
 std::cout << result << std::endl;
 }
 return 0;
}
```
Step4:
```c++
#include <iostream>
#include <string>
#include <regex>
int main() {
 std::regex regexp("( )\\1+");
 for (std::string line; std::getline(std::cin, line);) {
 std::string result;
 std::regex_replace(back_inserter(result), line.begin(), line.end(), regexp, "$1");
 std::cout << result << std::endl;
 }
 return 0;
}
```
Step5:
```c++
#include <iostream>
#include <string>
#include <regex>
int main() {
 std::regex regexp(" ");
 for (std::string line; std::getline(std::cin, line);) {
 std::string result;
 std::regex_replace(back_inserter(result), line.begin(), line.end(), regexp, "");
 std::cout << result << std::endl;
 }
 return 0;
}
```
Step6:
```c++
#include <iostream>
#include <string>
#include <regex>
int main() {
 std::regex regexp("\\/\\/.*");
 for (std::string line; std::getline(std::cin, line);) {
 std::string result;
 std::regex_replace(back_inserter(result), line.begin(), line.end(), regexp, "");
 std::cout << result << std::endl;
 }
 return 0;
}
```
Step7:
```c++
#include <iostream>
#include <string>
#include <regex>
int main() {
 int commentState = 0; 
 for (std::string line; std::getline(std::cin, line);) {
 std::string result;
 if (std::regex_match(line, std::regex("\\/\\*.*"))){
 std::regex regexp("\\/\\*.*");
 commentState = 1;
 std::regex_replace(back_inserter(result), line.begin(), line.end(), regexp, "");
 std::cout << result << std::endl;
 continue;
 }
 if (std::regex_match(line, std::regex("\\*\\/.*"))){
 std::regex regexp("\\*\\/");
 commentState = 0;
 std::regex_replace(back_inserter(result), line.begin(), line.end(), regexp, "");
 std::cout << result << std::endl;
 continue;
 }
 if (commentState == 1) {
 std::regex regexp(".");
 std::regex_replace(back_inserter(result), line.begin(), line.end(), regexp, "");
 std::cout << result << std::endl;
 } else {
 std::cout << line << std::endl;
 }
 }
 return 0;
}
```