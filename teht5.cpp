#include <iostream>
#include <string>
#include <regex>

int main() { 
    std::regex regexp(" "); 
    for (std::string line; std::getline(std::cin, line);) {
        std::string result;
        std::regex_replace(back_inserter(result), line.begin(), line.end(), regexp, "");
        std::cout << result << std::endl;
    }
    return 0;
}