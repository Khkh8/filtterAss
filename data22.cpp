#include <iostream>
#include <string>
#include <regex>
int main() {
 for (std::string line; std::getline(std::cin, line);) {
 // tarkistaa ettei ole tyhj?? rivi
 if (!std::regex_match(line, std::regex("^$")))
 std::cout << line << std::endl;
 }
 return 0;
}
