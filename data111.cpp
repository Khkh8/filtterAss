#include <iostream>
#include <string>
#include <regex>
int main() {
 std::regex const expressionWord("[^\\s]+");
 std::regex const expressionChar(".");
 std::regex const expressionWhitespace("[\\s]");
 int words = 0;
 int characters = 0;
 int lines = 0;
 int whitespaces = 0;
 for (std::string line; std::getline(std::cin, line);) {
 std::ptrdiff_t const match_count(std::distance(
 std::sregex_iterator(line.begin(), line.end(), expressionWord),
 std::sregex_iterator()));
 words += match_count;
 std::ptrdiff_t const match_count2(std::distance(
 std::sregex_iterator(line.begin(), line.end(), expressionChar),
 std::sregex_iterator()));
 characters += match_count2;
 std::ptrdiff_t const match_count3(std::distance(
 std::sregex_iterator(line.begin(), line.end(), expressionWhitespace),
 std::sregex_iterator()));
 whitespaces += match_count3;
 lines++;
 }
 std::cout << "Number of characters: " << characters << std::endl;
 std::cout << "Number of whitespaces: " << whitespaces << std::endl;
 std::cout << "Number of words: " << words << std::endl;
 std::cout << "Number of lines: " << lines << std::endl;
 return 0;
}
