#include <iostream>
#include <string>
#include <regex>

int main() { 
    std::regex regexp("( )\\1+"); 
    for (std::string line; std::getline(std::cin, line);) {
        std::string result;
        std::regex_replace(back_inserter(result), line.begin(), line.end(), regexp, "$1");
        std::cout << result << std::endl;
    }
    return 0;
}