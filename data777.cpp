#include <iostream>
#include <string>
#include <regex>
int main() {
 int commentState = 0; 
 for (std::string line; std::getline(std::cin, line);) {
 std::string result;
 if (std::regex_match(line, std::regex("\\/\\*.*"))){
 std::regex regexp("\\/\\*.*");
 commentState = 1;
 std::regex_replace(back_inserter(result), line.begin(), line.end(), regexp, "");
 std::cout << result << std::endl;
 continue;
 }
 if (std::regex_match(line, std::regex("\\*\\/.*"))){
 std::regex regexp("\\*\\/");
 commentState = 0;
 std::regex_replace(back_inserter(result), line.begin(), line.end(), regexp, "");
 std::cout << result << std::endl;
 continue;
 }
 if (commentState == 1) {
 std::regex regexp(".");
 std::regex_replace(back_inserter(result), line.begin(), line.end(), regexp, "");
 std::cout << result << std::endl;
 } else {
 std::cout << line << std::endl;
 }
 }
 return 0;
}
