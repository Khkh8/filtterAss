#include <iostream>
#include <string>
#include <regex>
int main() {
 // ehto sanalle
 std::regex const expressionWord("[^\\s]+");
 // ehto kirjaimelle
 std::regex const expressionChar(".");
 // ehto whitespacelle
 std::regex const expressionWhitespace("[\\s]");
 // SANAN M????RITELM??: m????rittelen sanan miksi tahansa merkiksi joita erottaa jokin whitespace
 // Esim. lasken yksitt??iset merkit kuten { my??s sanoiksi
 int words = 0; // laskee sanat
 int characters = 0; // laskee kirjaimet
 int lines = 0; // laskee rivit
 int whitespaces = 0; // laskee whitespacet
 for (std::string line; std::getline(std::cin, line);) {
 std::ptrdiff_t const match_count(std::distance(
 std::sregex_iterator(line.begin(), line.end(), expressionWord),
 std::sregex_iterator()));
 words += match_count;
 std::ptrdiff_t const match_count2(std::distance(
 std::sregex_iterator(line.begin(), line.end(), expressionChar),
 std::sregex_iterator()));
 characters += match_count2;
 std::ptrdiff_t const match_count3(std::distance(
 std::sregex_iterator(line.begin(), line.end(), expressionWhitespace),
 std::sregex_iterator()));
 whitespaces += match_count3;
 lines++;
 }
 std::cout << "Number of characters: " << characters << std::endl;
 std::cout << "Number of whitespaces: " << whitespaces << std::endl;
 std::cout << "Number of words: " << words << std::endl;
 std::cout << "Number of lines: " << lines << std::endl;
 return 0;
}
